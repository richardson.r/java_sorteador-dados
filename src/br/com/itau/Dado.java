package br.com.itau;
import java.util.Random;

public class Dado {
    private int numeros;
    private Random random;

    public int getNumeros() {
        return numeros;
    }
    public void setNumeros(int numeros) {
        this.numeros = numeros;
    }

    public Dado(int numeros){
        this.numeros = numeros;
        random = new Random();
    }

    public int sortear(){
        return random.nextInt(numeros) + 1;
    }
}
