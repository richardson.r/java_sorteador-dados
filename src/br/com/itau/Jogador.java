package br.com.itau;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Jogador {
    private ArrayList<Dado> dados = new ArrayList();

    public Jogador(){
        criarDados(3,6);
        sortearGrupos(3);
    }

    private void criarDados(int qtdDados, int qtdNumeros) {
        for(int i = 0; i < qtdDados; i++)
            this.dados.add(new Dado(qtdNumeros));
    }

    public void sortearGrupos(int quantidadeGrupos) {
        Map<Integer, ArrayList<Integer>> resultado = new HashMap<>();

        for (int i = 0; i < quantidadeGrupos; i++) {
            ArrayList<Integer> grupo = new ArrayList();
            int acumulador = 0;

            for (Dado dado : dados) {
                int numero = dado.sortear();
                acumulador += numero;
                grupo.add(numero);
            }
            grupo.add(acumulador);

            resultado.put(i, grupo);
        }

        IO.imprimirResultado(resultado);
    }
}
